/**
 * Created by nguyenluat on 2017/08/31.
 */
(function (self) {
  'use strict';
  
  //if (typeof notThisThenNext === 'undefined') {
  //  function notThisThenNext(a, b) {
  //    console.log('arguments: ', arguments);
  //    return a ? a : b;
  //  }
  //}
  
  if (self.notThenNext) {
    return;
  }
  
  
  self.undefinedThenNext = function () {
    if (arguments.length < 1) {
      throw new Error('Invalid arguments, undefinedThenNext require more than 1 args');
    }
    
    var returnValue = arguments[arguments.length - 1];
    for (var i = 0; i < arguments.length - 1; i++) {
      if (typeof arguments[i] !== 'undefined') {
        returnValue = arguments[i];
        break;
      }
    }
    
    return returnValue;
  }
  
  self.notThenNext = function () {
    if (arguments.length < 1) {
      throw new Error('Invalid arguments, notThenNext require more than 1 args');
    }
    
    var returnValue = arguments[arguments.length - 1];
    for (var i = 0; i < arguments.length - 1; i++) {
      if (typeof arguments[i] !== 'undefined' && arguments[i]) {
        returnValue = arguments[i];
        break;
      }
    }
    
    return returnValue;
  }
  
  self.notCallbackThenNext = function (checkCallback) {
    if (arguments.length < 1) {
      throw new Error('Invalid arguments, notCallbackThenNext require more than 2 args');
    }

    if (typeof checkCallback !== 'function') {
      throw new Error('notCallbackThenNext first arg must be a function, example: notCallbackThenNext(callback, arg1, arg2, ..., argN)');
    }
    
    var returnValue = arguments[arguments.length - 1];
  
    // Skip check callback as first arg
    for (var i = 1; i < arguments.length - 1; i++) {
      if (typeof arguments[i] !== 'undefined' && checkCallback(arguments[i])) {
        returnValue = arguments[i];
        break;
      }
    }
    
    return returnValue;
  }

  
  //self.notThenNext.polyfill = true; // What is this ?
})(typeof self !== 'undefined' ? self : this);
