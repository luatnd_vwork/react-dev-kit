import { combineReducers } from "redux";
import reduceReducers from "reduce-reducers";

import { i18nReducer } from "react-redux-i18n";
import { routerReducer } from 'react-router-redux';

/**
 * See this: This is latest version of a advance reducer
 * A standard reducer include:
 *    name: The reducer name,
 *    reducer: The redux reducer,
 *    reader: The reader of reducer, imagine redux reducer is how you write data to store, reader is how you read from the store
 *    persist: Persist configuration
 */
import {
  name as persistName,
  reducer as persistReducer,
  persist as persistPersist
} from './persistReducer';

import {
  name as productCategoryName,
  reducer as productCategoryReducer,
  persist as productCategoryPersist
} from "../../components/ProductCategory/ProductCategory.redux";
import {
  reducer as tmpShopItemDialogReducer,
  persist as tmpShopItemDialogPersist
} from "../../pages/ShopPage/TmpShopItemDialog/TmpShopItemDialog.redux";
import { reducer as topupReducer, persist as topupPersist } from "../../pages/ShopPage/Topup/Topup.redux";
import {
  reducer as checkoutPageReducer,
  persist as checkoutPagePersist
} from "../../pages/CheckoutPage/CheckoutPage.redux";
import { seoReducer } from "../../components/SEO/Seo.redux";


// TODO: Loop to auto-build rootReducer
export const rootReducer = reduceReducers(
  combineReducers({
    //reducerKey: reducer,
    [persistName]: persistReducer,
    i18n: i18nReducer,
    //auth: authReducer,
    router: routerReducer,
    [productCategoryName]: productCategoryReducer,
    tmpShopItemDialog: tmpShopItemDialogReducer,
    topup: topupReducer,
    checkoutPage: checkoutPageReducer,
    seo: seoReducer,
  })
);


/**
 * Set false to ignore persist that reducer
 * Set {} to ignore persist that reducer's key
 *
 * You can set ignore here, or set at `*.redux.ts` file like the tmpShopItemDialog
 * TODO: Loop to auto-build reducerPersists
 */
export const reducerPersists = {
  //reducerKey: reducerPersist,
  i18n: true,
  [persistName]: persistPersist,
  //auth: authReducer,
  router: false,
  [productCategoryName]: productCategoryPersist,
  tmpShopItemDialog: tmpShopItemDialogPersist, // nested persist info, defined in component reducer
  topup: topupPersist,
  checkoutPage: checkoutPagePersist,
  seo: true,
};