import { createAction, handleActions, combineActions } from "redux-actions";
import { combineReducers } from 'redux';
import { createRoutine } from 'redux-saga-routines';

import { reducer as choosePaymentReducer, persist as choosePaymentPersist } from './ChoosePayment/ChoosePayment.redux';
import { doPaymentReducer } from './DoPayment/DoPayment.redux';

/**
 * Useful documentation:
 *
 * https://redux-actions.js.org/docs/api/combineActions.html
 * https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
 * https://github.com/redux-saga/redux-saga/tree/master/docs/api#callfn-args
 * https://github.com/afitiskin/redux-saga-routines
 *
 */

/**
 * STEP 0: Include you reducer to redux: `src/base/redux/rootReducer.js`
 *
 * STEP 1: Initial state
 */
const initialState:any = {
  step: 1,
  paymentMethod: null,
};


/**
 * STEP 2: Define redux action, two kind of action:
 *    1. Non HTTP request: Use redux-actions.createAction to create action, called `redux action`
 *    2. HTTP request: Use redux-saga-routines.createRoutine to create action, called `saga routines`
 *
 *
 * Naming rule (<mean required> [mean optional]):
 * Format:      <Action> [Addition]  <ReducerName>   [Addition of Reducer]
 * Example:     HIDE                  NOTIFICATION
 * Example:     SET                   NOTIFICATION    _ATTRS
 * Example:     SHOW      TEXT        NOTIFICATION
 *
 * If your action is Non-HTTP request, must append `saga/` prefix to your action name: eg: `saga/FETCH_USER`
 *
 * WHY: You know why? --> To segregate which is redux action, which is saga routine for easy handle in the future
 *
 *
 * Now, you can use your action in your React Component (refer to whatever component to see the real example):
 * @connect(
 *     state => ({}),
 *     getAllMapper({
 *       sagaAction,
 *     }, {
 *       reduxAction,p
 *     })
 * )
 */
// export const setCheckoutCommonInfo:any = createAction('SET_CHECKOUT_COMMON_INFO', (option:any) => option);

/**
 * STEP 3: Describe redux reducer, no saga related here
 */
// export const paymentCommonReducer = handleActions({
//   [setCheckoutCommonInfo]: (state, action) => ({...state, ...action.payload}),
// }, {...initialState});


// Root payment reducer
export const reducer = combineReducers({
  // common: paymentCommonReducer,
  choosePayment: choosePaymentReducer,
  doPayment: doPaymentReducer,
})

export const persist = {
  choosePayment: choosePaymentPersist,
  doPayment: false, // Do not allow save to local db
}