import React from 'react';
import { connect } from "react-redux";
import classnames from 'classnames';
import { Layout, Breadcrumb, BackTop } from 'antd';

import MyLayout from '../Layout';
import { getDispatchMapper } from '../../base/redux/dispatch';

const CommonStyles = require("../../components/Styles/Styles.module.scss");
const Styles = require("./HomePage.module.scss");

@connect(
  state => ({}),
  getDispatchMapper({
    
  })
)
export default class HomePage extends React.Component {
  static propTypes = {
    
  }
  
  reloadClientBrowser = () => {
    setTimeout(function () {
      window.location.reload()
    }, 8000);
  }
  
  render() {
    const { Content } = Layout;
    
    return (
    <MyLayout>
      <Content className={classnames([
        Styles.mainContainer,
        CommonStyles.antBsContainer,
      ])}>
        <Breadcrumb style={{ marginBottom: '12px'}}>
          <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        </Breadcrumb>
        
        <div>
          <h2>Tự động quay lại trang chủ sau vài giây nữa..</h2>
          {this.reloadClientBrowser()}
        </div>
  
        <BackTop>
          <div className="ant-back-top-inner">UP</div>
        </BackTop>
      </Content>
    </MyLayout>
    );
  }
}
