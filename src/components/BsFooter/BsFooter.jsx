import React from 'react';

require("./BsFooter.module.scss");


export default class BsFooter extends React.Component {
  render() {
    return (
      <footer className="black-bg" id="contact" style={{marginTop: '20px'}}>
        <div className="container">
          <div className="space-40"></div>
          <div className="row text-white wow fadeIn" style={{visibility: 'visible', animationName: 'fadeIn'}}>
            <div className="container">
              <div className="row">
                <div className="col-md-5 col-sm-6 footerleft ">
                  <div className="logofooter"><img src="https://www.campp.vn/uploads/campp-logo-1.png" alt="Campp logo"/></div>
                  <p>Campus + không chỉ đồng hành cùng sinh viên khi còn đang học tại các trường đại học, mà còn hỗ trợ,
                    tìm kiếm những cơ hội cho sinh viên sau khi ra trường.</p>
                  <p><i className="fa fa-map-pin"></i>
                    Tầng số 05, tòa nhà Viện Dầu Khí, số 167 Trung Kính, phường Yên Hòa, quận Cầu Giấy, thành phố Hà
                    Nội, Việt Nam
                  </p>
                  <p><i className="fa fa-phone"></i> Phone : 02438 554 666</p>
                  <p><i className="fa fa-envelope"></i> E-mail: info@campp.vn</p>
                </div>
                <div className="col-md-7 col-sm-6 paddingtop-bottom"><h6 className="heading7">Thông tin</h6>
                  <ul className="footer-ul">
                    <li><a href="#"> Chính sách &amp; Quy định chung</a></li>
                    <li><a href="#"> Quy định và hình thức thanh toán</a></li>
                    <li><a href="#"> Chính sách vận chuyển/giao nhận/cài đặt</a></li>
                    <li><a href="#"> Chính sách bảo hành/bảo trì</a></li>
                    <li><a href="#"> Chính sách đổi/trả hàng và hoàn tiền</a></li>
                    <li><a href="#"> Chính sách bảo mật thông tin cá nhân</a></li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div className="col-xs-12 text-center text-white" style={{color: '#999'}}>
              <div className="space-20"></div>
              <p>Đơn vị chủ quản: Công ty cổ phần công nghệ Bee Tech Việt Nam</p>
              <p>Người đại diện: Ông Nguyễn Phương Ngọc - Giám đốc công ty</p>
            </div>
          </div>
          
          <div className="space-20"></div>
        </div>
      </footer>
    )
  }
}