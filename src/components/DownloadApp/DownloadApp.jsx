import React from 'react'

import Styles from "./DownloadApp.module.scss"

import appStoreAndroid from '../../assets/img/appstore-download-android.png';
import appStoreIos from '../../assets/img/appstore-download-ios.png';


export default class DownloadApp extends React.Component {
  render() {
    return (
      <div className={Styles.container}>
        {this.props.children}
      
        <div className={Styles.btnSection}>
          <a href="https://play.google.com/store/apps/details?id=vn.beetech.campp" target="_blank" rel="noopener noreferrer">
            <img className={Styles.img} src={appStoreAndroid} alt="Download campus cộng trên play store"/>
          </a>
        
          <a href="https://itunes.apple.com/us/app/campp-c%E1%BB%99ng-k%E1%BA%BFt-n%E1%BB%91i-sinh-vi%C3%AAn-vi%E1%BB%87t/id1265119655?ls=1&mt=8" target="_blank" rel="noopener noreferrer">
            <img className={Styles.img} src={appStoreIos} alt="Download campus cộng trên apple store"/>
          </a>
        </div>

      </div>
    );
  }
}
